from sklearn.ensemble import AdaBoostClassifier
from sklearn.datasets import make_classification
import pandas as pd
import os
import numpy as np
from logitboost import LogitBoost
from sklearn.tree import DecisionTreeRegressor

def get_folds(m, folds):
    perm=np.random.permutation(m)
    d=m//folds
    r= m-d*folds
    flds=[]
    for i in range(folds):
        start=i*d
        end=(i+1)*d
        if i<r:
            end=end+1
        flds.append(perm[start:end])
    return flds

os.chdir('C:\\Users\\Natalie Frank\\Documents\\Foundations of Machine Learning\\homework\\HW3\\Code')
df=pd.read_csv('abalone.data',header=None)
df.columns=['Sex','Length','Diameter', 'Height','Whole','Shucked','Viscera','Shell','Rings']
ls=df.shape
rows=ls[0]
y=[0]*rows
male=[0]*rows
female=[0]*rows
infant=[0]*rows
for r in range(rows):
    if (df.iloc[r,8]<10) & (df.iloc[r,8]>0): 
        y[r]=1
    if df.iloc[r,0]=="M":
        male[r]=1
    elif df.iloc[r,0]=="F":
        female[r]=1  
    elif df.iloc[r,0]=="I":
        infant[r]=1    


df=df.drop('Sex',axis=1)
df=df.drop('Rings',axis=1)

df['Female']=female
df['Male']=male
df['Infant']=infant


x_train=df.iloc[:3133,:]
x_test=df.iloc[3133:,:]

y=pd.DataFrame({'col':y})

y_train=y.iloc[:3133,:]
y_test=y.iloc[3133:,:]

folds=10
flds=get_folds(len(x_train),folds)

mx=10
ada=[0]*mx
ada_mn=float('-inf')
ada_mn_ind=-1

logistic=[0]*mx
log_mn=float('-inf')
log_mn_ind=-1

for method in range(2):# 0 for ada 1 for logisitic
    for t in range(mx): #cv loop
        T=100*(t+1)
        if method==0:
            clf=AdaBoostClassifier(n_estimators=T,random_state=22)
        else:
            clf = LogitBoost(DecisionTreeRegressor(max_depth=1),n_estimators=T, random_state=0)
        mn=0
        for f in range(folds):
            x_test_cv=x_train.iloc[flds[f],:]
            y_test_cv=y_train.iloc[flds[f]]

            x_train_cv=x_train.drop(flds[f])
            y_train_cv=y_train.drop(flds[f])

            clf.fit(x_train_cv.to_numpy(),y_train_cv.to_numpy().ravel())
            mn=mn+clf.score(x_test_cv.to_numpy(),y_test_cv.to_numpy())
            if method==0:
                d=0
            else:
                d=0
        mn=mn/10
        if method==0:
            ada[t]=mn
            if mn> ada_mn:
                ada_mn=mn
                ada_mn_ind=t+1
        else:
            logistic[t]=mn
            if mn> log_mn:
                log_mn=mn
                log_mn_ind=t+1



clf=AdaBoostClassifier(n_estimators=ada_mn_ind*100,random_state=22)
clf.fit(x_train_cv,y_train_cv.to_numpy().ravel())
scr= clf.score(x_test_cv,y_test_cv)
print("AdaBoost score is %a " %scr)
print("all adaboost scores:")
print(ada)
print(ada_mn_ind)

print("")

clf=LogitBoost(DecisionTreeRegressor(max_depth=1),n_estimators=log_mn_ind*100, random_state=0)
clf.fit(x_train_cv,y_train_cv.to_numpy().ravel())
scr= clf.score(x_test_cv,y_test_cv)
print("LogisticBoost score is %a " %scr)
print("all logisticboost scores:")
print(logistic)
print(log_mn_ind)


